from orbit.orbit_orm import BaseTable, BaseCollection
from pynndb import SerialiserType, Doc
from loguru import logger as log


class ProjectTable (BaseTable):

    norm_table_name = 'projects'
    norm_auditing = True
    norm_codec = SerialiserType.UJSON
    norm_ensure = [
        {'index_name': 'by_params', 'func': '{provider}|{project}'},
    ]

    def from_params (self, params, transaction=None):
        doc = Doc(params)
        self.set(self.norm_tb.seek_one('by_params', doc, txn=transaction))
        if not self.isValid:
            self.set(doc)
        return self


class ProjectCollection (BaseCollection):

    table_class = ProjectTable
    table_methods = ['get_ids']

    async def put (self, params):
        doc = self.table_class().from_params(params)
        if doc.isValid:
            log.debug(f'Update')
            doc.update(params).save()
        else:
            log.debug(f'Append')
            doc.update(params).append()
        return {'ok': True}

    async def remove (self, params):
        doc = self.table_class().from_params(params)
        if not doc.isValid:
            return {'ok': False, 'error': f'Project entry not found: {id}'}
        doc.delete()
        return {'ok': True}
