from orbit.orbit_orm import BaseTable, BaseCollection, register_class, register_method
from pynndb import SerialiserType, Doc
from hashlib import md5
from orbit.orbit_shared import world
from loguru import logger as log
from gitlab import Gitlab
from base64 import b64decode, b64encode

gitlab = Gitlab()


class FileCacheTable (BaseTable):

    norm_table_name = 'filecache'
    norm_auditing = True
    norm_codec = SerialiserType.UJSON
    norm_ensure = [
        {'index_name': 'by_url', 'func': '{url}'},
        {'index_name': 'by_params', 'func': '{provider}|{project}|{path}'}
    ]

    @property
    def cache_path (self):
        return world.conf.tmp / md5(self._url.encode()).hexdigest()

    def from_url (self, url, transaction=None):
        self.set(self.norm_tb.seek_one('by_url', Doc({'url': url}), txn=transaction))
        return self
    
    def from_cache (self):
        try:
            with open(self.cache_path, 'r') as io:
                return io.read()
        except FileNotFoundError:
            return None

    def to_cache (self, text):
        with open(self.cache_path, 'w') as io:
            io.write(text)

    def from_params (self, params, transaction=None):
        doc = Doc(params)
        self.set(self.norm_tb.seek_one('by_params', doc, txn=transaction))
        if not self.isValid:
            self.set(doc)
        return self
    
    async def fetch (self):
        project = gitlab.projects.list(search=self._project)
        data = project.repository_blob(self.key)
        return b64decode(data.get('content','')) if data.get('encoding') else data.get('content')


@register_class
class FileCacheCollection (BaseCollection):

    table_class = FileCacheTable
    table_methods = ['get_ids']

    @register_method
    def get_urls(cls, session, params, transaction=None):
        ids, data = [], []
        log.warning(f"URLS> {params.get('urls')}")
        for url in params.get('urls', []):
            log.success(f'url = {url}')
            doc = cls.table_class().from_url(url)
            if doc.isValid:
                session.append(params, doc.key, ids, data, doc, strip=cls.table_strip)
            else:
                log.error('not found')
        session.update(ids, params)
        return {'ok': True, 'ids': ids, 'data': data}

    # project.repository_blob(id) => recover item