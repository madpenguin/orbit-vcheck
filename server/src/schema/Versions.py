from orbit.orbit_orm import BaseTable, BaseCollection
from schema.MySessions import MySessionsCollection
from pynndb import SerialiserType, Doc
from datetime import datetime


class VersionsTable (BaseTable):

    norm_table_name = 'versions'
    norm_auditing = True
    norm_codec = SerialiserType.UJSON
    norm_ensure = [
        {'index_name': 'by_product', 'func': '{product}'},
    ]

    @property
    def last_seen (self):
        return datetime.utcfromtimestamp(self._when).strftime('%Y-%m-%d %H:%M:%S')

    def from_product (self, product, transaction=None):
        self.set(self.norm_tb.seek_one('by_product', Doc({'product': product}), txn=transaction))
        return self


class VersionsCollection (BaseCollection):

    table_class = VersionsTable
    table_methods = ['get_ids']

    async def get_version (self, product, version):
        doc = VersionsTable().from_product(product)
        if doc.isValid:
            await MySessionsCollection(self._sid).update_product(product, version)
            return doc._version
        return 'unknown'
