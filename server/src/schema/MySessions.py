from orbit.schema.Sessions import SessionsTable, SessionsCollection
from loguru import logger as log
from orbit.orbit_shared import world


class MySessionsTable (SessionsTable):
    pass


class MySessionsCollection (SessionsCollection):

    table_methods = ['get_ids']

    async def update_product (self, product, version):
        doc = MySessionsTable().from_sid(self._sid)
        if not doc.isValid:
            log.error(f'SESSION IS INVALID: {self._sid}')
        else:
            doc.update({'product': product, 'version': version}).save()
