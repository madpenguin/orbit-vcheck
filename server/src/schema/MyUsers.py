from orbit.orbit_shared import world
from orbit.schema.Users import UsersTable, UsersCollection
from datetime import datetime
from loguru import logger as log


class MyUsersTable (UsersTable):

    @property
    def last_seen (self):
        return datetime.utcfromtimestamp(self._when).strftime('%Y-%m-%d %H:%M:%S')
    
    @property
    def code (self):
        return self._code if self._code else "Activated"
    

class MyUsersCollection (UsersCollection):

    table_methods = ['get_ids']

    async def update_version (self, version):
        session = await world.api.get_session(self._sid)
        doc = MyUsersTable().from_key(session.get('host_id'))
        if not doc.isValid:
            log.error(f'USER IS INVALID: {session.get("host_id")}')
        else:
            doc.update({'version': version}).save()
