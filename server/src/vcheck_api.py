from orbit.orbit_api import OrbitAPI
from orbit.orbit_database import OrbitDatabase
from orbit.orbit_decorators import navGuard
from schema.MyUsers import MyUsersCollection
from schema.MySessions import MySessionsCollection
from schema.Versions import VersionsCollection
from schema.FileCache import FileCacheCollection
from schema.Cache import CacheCollection
from schema.APIs import APIsCollection
from schema.Project import ProjectCollection
from vcheck_filecache import AsyncFileCache
from loguru import logger as log


class VCheckAPI (OrbitAPI):

    def __init__ (self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._cache = AsyncFileCache()

    def open (self, *args, **kwargs):
        OrbitDatabase.COLLECTIONS = [
            MyUsersCollection,
            MySessionsCollection,
            VersionsCollection,
            FileCacheCollection,
            CacheCollection,
            APIsCollection,
            ProjectCollection,
        ]
        super().open(*args, **kwargs)

    @navGuard
    async def on_cache_fetch (self, sid, params, force=False):
        return await CacheCollection(sid).fetch(params, force)

    @navGuard
    async def on_get_project_id (self, sid, params):
        return await CacheCollection(sid).get_project_id(params)

    @navGuard
    async def on_cache_put (self, sid, old_data, new_data):
        return await CacheCollection(sid).put(old_data, new_data)

    @navGuard
    async def on_project_put (self, sid, params):
        return await ProjectCollection(sid).put(params)

    @navGuard
    async def on_project_remove (self, sid, params):
        try:
            await ProjectCollection(sid).remove(params)
            await CacheCollection(sid).remove(params)
            return {'ok': True}
        except Exception as e:
            log.exception(e)
            return {'ok': False, 'error': str(e)}

    @navGuard
    async def on_get_version (self, sid, product, version):
        return {'ok': True, 'version': await VersionsCollection(sid).get_version(product, version) }

    # @navGuard
    # async def on_cache_fetch (self, sid, url):
    #     return await self._cache.fetch(url)

    @navGuard
    async def on_cache_ensure (self, sid, url):
        return await self._cache.ensure(url)

    @navGuard
    async def on_join(self, sid, room):
        return self.enter_room(sid, room)

    @navGuard
    async def on_get_api_remote (self, sid, provider, api, path):
        log.error(f"Get {sid} {provider} {api} {path}")
        return APIsCollection(sid).get_api_remote(provider, api, path)
