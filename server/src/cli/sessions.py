from orbit.orbit_config import OrbitConfig
from orbit.orbit_database import OrbitDatabase
from orbit.orbit_shared import world
from schema.MySessions import MySessionsCollection, MySessionsTable
from rich.console import Console
from rich.table import Table


class Sessions():

    def __init__ (self, app):
        self._app = app

    def list (self):
        world.conf = OrbitConfig(self._app).open()
        OrbitDatabase.COLLECTIONS = [MySessionsCollection]
        OrbitDatabase().open()
        table = Table(
            title='Current Sessions',
            title_style='bold green',
            caption=f'authentication="{world.conf.authentication}"',
            caption_style='bold magenta'
        )
        rstyle = 'cyan'
        hstyle = 'deep_sky_blue4'
        table.add_column('Session Id', style=rstyle, header_style=hstyle, no_wrap=True)
        table.add_column('Host Id',    style=rstyle, header_style=hstyle, no_wrap=True)
        table.add_column('Start Time', style=rstyle, header_style=hstyle, no_wrap=True)
        table.add_column('Vendor',     style=rstyle, header_style=hstyle, no_wrap=True)
        table.add_column('Platform',   style=rstyle, header_style=hstyle, no_wrap=True)
        table.add_column('Language',   style=rstyle, header_style=hstyle, no_wrap=True)
        table.add_column('Client',     style=rstyle, header_style=hstyle, no_wrap=True)
        for result in MySessionsCollection():
            doc = result.doc
            table.add_row(
                doc._sid,
                doc._host_id,
                doc.when,
                doc._vendor,
                doc._platform,
                doc._language,
                doc._product)
        Console().print(table)
