from orbit.orbit_config import OrbitConfig
from orbit.orbit_shared import world
from orbit.orbit_database import OrbitDatabase
from schema.Versions import VersionsCollection, VersionsTable
from rich.console import Console
from rich.table import Table
from datetime import datetime


class Versions():

    def __init__ (self, app):
        self._app = app

    def run (self):
        world.conf = OrbitConfig(self._app).open()
        OrbitDatabase.COLLECTIONS = [VersionsCollection]
        OrbitDatabase().open()

    def run_list (self):
        self.run()
        table = Table(
            title='Registered Versions',
            title_style='bold green',
        )
        rstyle = 'cyan'
        hstyle = 'deep_sky_blue4'
        table.add_column('Product Name',    style=rstyle, header_style=hstyle, no_wrap=True)
        table.add_column('Version String',  style=rstyle, header_style=hstyle, no_wrap=True)
        table.add_column('Last Updated',    style=rstyle, header_style=hstyle, no_wrap=True)

        for result in VersionsCollection():
            doc = result.doc
            table.add_row(
                doc._product,
                doc._version,
                doc.last_seen)
        Console().print(table)

    def run_add (self, product, version):
        self.run()
        doc = VersionsTable().from_product(product)
        doc.update({'version': version, 'when': datetime.now().timestamp()})
        if doc.isValid:
            old_version = doc._version
            doc.save()
            print(f'product ({product}) updated from {old_version} to {version}')
        else:
            doc.update({'product': product})
            doc.append()
            print(f'product ({product}) version ({version}) appended')