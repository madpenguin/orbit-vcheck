from orbit.orbit_config import OrbitConfig
from orbit.orbit_database import OrbitDatabase
from orbit.orbit_shared import world
from orbit.schema.Users import UsersCollection
from schema.MyUsers import MyUsersTable
from rich.console import Console
from rich.table import Table


class Users():

    def __init__ (self, app):
        self._app = app

    def run_list (self):
        world.conf = OrbitConfig(self._app).open()
        OrbitDatabase().open()

        table = Table(
            title='Registered Users',
            title_style='bold green',
            caption=f'authentication="{world.conf.authentication}"',
            caption_style='bold magenta'
        )
        rstyle = 'cyan'
        hstyle = 'deep_sky_blue4'
        table.add_column('User Id',     style=rstyle, header_style=hstyle, no_wrap=True)
        table.add_column('Active',      style=rstyle, header_style=hstyle, no_wrap=True)
        table.add_column('Code',        style=rstyle, header_style=hstyle, no_wrap=True)
        table.add_column('Tries',       style=rstyle, header_style=hstyle, no_wrap=True)
        table.add_column('Version',     style=rstyle, header_style=hstyle, no_wrap=True)
        for result in UsersCollection():
            doc = MyUsersTable(result.doc.doc)
            table.add_row(
                doc._user_id,
                str(doc._active),
                doc.code,
                str(doc._tries),
                doc._version)            
        Console().print(table)
