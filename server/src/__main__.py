#!/usr/bin/env python

from argparse import ArgumentParser
from orbit.orbit_app import OrbitAPP
from orbit.orbit_router import OrbitRouter
from orbit.orbit_shared import world
from orbit.orbit_version import __version__ as orbit_version
from version import __version__
from vcheck_api import VCheckAPI
from vcheck_config import VCheckConfig
from cli.users import Users
from cli.versions import Versions
from cli.sessions import Sessions
from sys import exit
from multiprocessing import freeze_support, set_start_method

APPLICATION = 'orbit-vcheck'

if __name__ == '__main__':
    set_start_method('spawn')
    freeze_support()

    parser = ArgumentParser()
    parser.add_argument("--dev", action='store_true', default=False)
    parser.add_argument("--run", action='store_true', default=False)
    parser.add_argument("--version", action='store_true')
    parser.add_argument("--list-users", action='store_true')
    parser.add_argument("--list-versions", action='store_true')
    parser.add_argument("--list-sessions", action='store_true')
    parser.add_argument("--add", action='store_true')
    parser.add_argument("--vstring", type=str)
    parser.add_argument("--product", type=str)
    world.args = parser.parse_args()
    if world.args.version:
        print(f'Versions, orbit={orbit_version}, vcheck={__version__}')
        exit()
    if world.args.list_users:
        Users(APPLICATION).run_list()
        exit()
    if world.args.list_versions:
        Versions(APPLICATION).run_list()
        exit()
    if world.args.list_sessions:
        Sessions(APPLICATION).list()
        exit()
    if world.args.add:
        if not world.args.product or not world.args.vstring:
            parser.error('You need to specify both product and vstring arguments to add a new version')
        else:
            Versions(APPLICATION).run_add(world.args.product, world.args.vstring)
        exit()
    if world.args.run:
        OrbitAPP(APPLICATION, VCheckConfig, VCheckAPI, OrbitRouter).run()
    else:
        parser.error('Please add "run" if you wish to launch the application')
