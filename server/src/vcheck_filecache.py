from datetime import datetime
from dateutil.parser import parse as parsedate
from orbit.orbit_shared import world
from loguru import logger as log
from cmarkgfm import github_flavored_markdown_to_html
from cmarkgfm.cmark import Options as opts
from asyncio import ensure_future
from aiohttp import ClientSession
from schema.FileCache import FileCacheTable
from nh3 import clean
from pygments import highlight
from pygments.lexers import get_lexer_by_name
from pygments.formatters import HtmlFormatter
from pygments.styles import get_style_by_name
from bs4 import BeautifulSoup

"""demo_filecache.py
Module to recover (.md) files and cache them locally, making them available as
sanitised HTML chunks via the RPC(fetch) method.
"""

class AsyncFileCache:

    MD_OPTIONS = (opts.CMARK_OPT_UNSAFE | opts.CMARK_OPT_LIBERAL_HTML_TAG | opts.CMARK_OPT_DEFAULT)

    def __init__ (self):
        if not world.conf.tmp.exists():
            world.conf.tmp.mkdir(parents=True, exist_ok=True)

    async def fetch (self, url):
        doc = FileCacheTable().from_url(url)
        if doc.isValid:
            text = doc.from_cache()
            if text:
                ensure_future(self.check(doc))
                return { 'ok': True, 'text': text, 'etag': doc._etag }
        doc._url = url
        text = await self.update(doc)
        return { 'ok': True, 'text': text, 'etag': doc._etag }
    
    async def ensure (self, url):
        doc = FileCacheTable().from_url(url)
        if doc.isValid:
            text = doc.from_cache()
            if text:
                ensure_future(self.check(doc))
                return { 'ok': True }
        doc._url = url
        await self.update(doc)
        return { 'ok': True }

    async def update (self, doc):
        async with ClientSession() as session:
            async with session.get(doc._url) as response:
                self.update_cache(doc, await response.text())
                self.update_store(doc, response)

    def update_cache (self, doc, text):
        # FIXME: clean sounds great, but removes IMG STYLE tags
        html = github_flavored_markdown_to_html(text, self.MD_OPTIONS)
        formatter = HtmlFormatter(style='monokai')
        soup = BeautifulSoup(html, 'html.parser')
        for tag in soup.find_all('pre'):
            lexer = get_lexer_by_name(tag.get('lang'), stripall=True)
            tag.replace_with(BeautifulSoup(highlight(tag.find('code').text, lexer, formatter), 'html.parser'))
        html = '<style>' + formatter.get_style_defs() + '</style>' + str(soup)
        doc.to_cache(html)

    def update_store (self, doc, response):
        log.success('Update Store')
        etag = response.headers.get('etag').strip('"')
        doc.update({'etag': etag})
        log.error(f'Updating ETAG-> {etag}')
        doc.save() if doc.isValid else doc.append()

    async def check (self, doc):
        async with ClientSession() as session:
            async with session.head(doc._url) as response:
                if 'Last-Modified' in response.headers:
                    url_date = parsedate(response.headers['Last-Modified']).astimezone()
                    file_date = datetime.datetime.fromtimestamp(doc.cache_path.getmtime(doc.cache_path)).astimezone()
                    if(url_date <= file_date):
                        log.debug(f'Serving "{doc.cache_path}" from cache based on last-modified')
                        return
                elif 'Etag' in response.headers:
                    etag = doc._etag if doc.isValid else None
                    if etag and response.headers['etag'].strip('"') == etag:
                        log.debug(f'Serving "{doc.cache_path}" from cache based on Etag')
                        return
                log.warning("Check did an update")
                await self.update(doc)
