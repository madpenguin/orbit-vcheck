from orbit.orbit_config import OrbitConfig
from orbit.orbit_database import OrbitDatabase
from orbit.orbit_shared import world
from schema.MySessions import MySessionsCollection, MySessionsTable
from schema.Cache import CacheCollection, CacheTable
from asyncio import sleep
from pynndb import Doc
from loguru import logger as log
import pytest

DELAY = 0.1
PROVIDER = 'gitlab'
PROJECT = 'orbit-vcheck'
PROJECT_ID = 44869863

@pytest.fixture
async def db():

    world.conf = OrbitConfig('unit_tests').open()
    db = OrbitDatabase([CacheCollection]).open()
    CacheCollection().empty()
    yield db
    db.close()


@pytest.mark.asyncio
async def test_fetch (db):

    results = []

    async def monitor(docs):
        nonlocal results
        results += docs

    async for database in db:
        CacheTable().norm_tb.watch(callback=monitor)
        result = await CacheCollection().get_project_id(PROVIDER, PROJECT)
        project_id = result.get('id')
        log.info(f'Project ID={project_id}')
        assert project_id == PROJECT_ID
        
        a = await CacheCollection().fetch({'provider': PROVIDER, 'project_id': project_id, 'path': 'README.md'})
        b = await CacheCollection().fetch({'provider': PROVIDER, 'project_id': project_id, 'path': 'README.md'})
        assert a==b, 'live and cache not the same!'
        await sleep(2)
        # keys = []
        # lower = Doc({'path': '/'})
        # upper = Doc({'path': f'/{chr(255)}'})
        # for result in APIsCollection().filter(index_name='by_parent', lower=lower, upper=upper):
        #     keys.append(result.key)

        # assert keys == [b'|LICENSE.md', b'|README.md', b'|client', b'|screenshots', b'|server'], 'root error'
        # await sleep(DELAY)
        # assert len(results) == 28, 'audit failure'