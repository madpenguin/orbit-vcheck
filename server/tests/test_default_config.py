from src.vcheck_config import VCheckConfig

def test_default_config ():

    config = VCheckConfig('orbit-vcheck').open()

    assert config.authentication == 'autoenroll', 'authentication override failed'