md="""# Orbit Versioncheck

This application has two purposes. Firstly it serves as a version notification and tracking
system for Orbit Communicator (or indeed any Orbit Framework based application) and secondly
it's a demonstration of how to write an Orbit Framework based application.

## What does it do?

It consists of a backend (written in Python) that provides a websockets API interface that
allows applications to discover the latest version of any published software components. Moreover
it allows applications to maintain a persistent connection over which they can be notified in 
real time when a new version becomes available. Orbit communicator for example will pop an
update button immediately a new version becomes available.

It also provides a User Interface (which can be made public, or not) which allows people to see
what's going on while providing a dynamic demonstration of some of the things the Framework can
facilitate.

<img style="height:260px" src="https://gitlab.com/madpenguin/orbit-vcheck/-/raw/main/screenshots/vcheck2.png" />
<img style="height:260px" src="https://gitlab.com/madpenguin/orbit-vcheck/-/raw/main/screenshots/vcheck1.png" />

## Other features

The <b>Home</b> page used a caching facility that fetches a <b>Markdown</b> page directly from
a source repository and renders it in HTML. (which would be <i>THIS</i> page) This is handy from
the perspective of reducing documentation, and indeed making documentation easier to write and
maintain. In this case I've written a README.md for the version-check project, which can then
be rendered as an online home page via an Orbit application.

## ToDo

* Check inline update on new version
* Async load API: from gitlab repo tree
* Add "last seen" to users
* Sidebar / CPU
* Webhooks for dynamic updates
* Need a fix for images / set width/height

curl https://api.github.com/repos/lassana/continuous-audiorecorder/contents/"""

"""demo_filecache.py

Module to recover (.md) files and cache them locally, making them available as
sanitised HTML chunks via the RPC(fetch) method.
"""

from loguru import logger as log
from cmarkgfm import github_flavored_markdown_to_html
from cmarkgfm.cmark import Options as opts
from nh3 import clean
from pygments import highlight
from pygments.lexers import get_lexer_by_name
from pygments.formatters import HtmlFormatter
from pygments.styles import get_style_by_name
from bs4 import BeautifulSoup

def test_default_parser ():
        
        MD_OPTIONS = (opts.CMARK_OPT_UNSAFE | opts.CMARK_OPT_LIBERAL_HTML_TAG | opts.CMARK_OPT_DEFAULT)

        html = github_flavored_markdown_to_html(md, MD_OPTIONS)
        print(html)
        formatter = HtmlFormatter(style='monokai')
        soup = BeautifulSoup(html, 'html.parser')
        for tag in soup.find_all('pre'):
            lexer = get_lexer_by_name(tag.get('lang'), stripall=True)
            tag.replace_with(BeautifulSoup(highlight(tag.find('code').text, lexer, formatter), 'html.parser'))
        html = '<style>' + formatter.get_style_defs() + '</style>' + str(soup)
        print("----")
        print(html)