from typing import Optional, Type

class Test:
    def __init__(self, database: Database, name: str, cls: Optional[ORMClass]=None) -> None:
        """
        Intantiate a table instance bases on the name of the table required. A
        reference to the containing database is also required so the table
        can back-reference the database environment.

        database - a reference to the containing database object
        name - the name of the table to reference
        cls - the ORM class used to represent items fom this table
        """
        pass
