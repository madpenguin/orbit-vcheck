all:
	@echo ".."
	@echo "make build"
	@echo ".."

build:
	@echo "New Build"
	@./scripts/roll_version.py
	@rm -rf apt/usr/local/bin
	@rm -rf apt/opt/orbit-vcheck/web
	@rm -rf apt/opt/orbit-vcheck/scripts
	@umask 0022
	@mkdir -p apt/usr/local/bin
	@mkdir -p apt/opt/orbit-vcheck/
	@mkdir -p apt/opt/orbit-vcheck/web
	@mkdir -p apt/opt/orbit-vcheck/scripts
	@cd server; make dist
	@cd client; make dist
	@cp server/scripts/make_keys.sh apt/opt/orbit-vcheck/scripts/
	@cp -r server/dist/orbit_vcheck apt/opt/orbit-vcheck/
	@cp -r client/dist/* apt/opt/orbit-vcheck/web
	@cd apt/usr/local/bin && ln -sf /opt/orbit-vcheck/orbit_vcheck .
	@dpkg-deb --build --root-owner-group apt dist

backup:
	tar cvfz /tmp/orbit-vcheck.tgz \
	--exclude=node_modules \
	--exclude=node_modules.old \
	--exclude="*.mdb" \
	--exclude="dist" \
	--exclude="build" \
	--exclude=".cache" \
	--exclude="*.log" \
                --exclude="*.zip" \
                --exclude=".random_errors" \
                --exclude=".pytest*" \
                --exclude="*__pycache__*" \
                --exclude=".attic" \
                --exclude=".tox" \
                --exclude=".venv"  \
                --exclude=".crap"  \
                --exclude="htmlcov"  \
                --exclude=".git" \
                --exclude=".thumbnail_cache" \
                --exclude=".parts_cache" \
                .
