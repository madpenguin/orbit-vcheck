import { defineStore } from 'pinia'

export const useSessionsStore = defineStore('sessionsStore', {
    state: () => {
        return {
            model: 'sessions'
        }
    },
    actions: {
        sort (params) {},
        populate (callback=null) {
            const label = 'populate'
            if (label in this.local_ids) {
                console.log(`query already active: ${this.model}/${label}`)
                return
            }
            const method = `api_${this.model}_get_ids`
            this.query({
                model: this.model,
                label: label,
                method: method,
                filter: {
                    index_name: 'by_when',
                    reverse: true
                }
            },
                (response) => {
                    if (!response || !response.ok)
                        throw new Error(response ? response.toString : `no query: ${method}`)
                    else console.log('Populate Sessions>', response)
                    if (callback) callback(response)
                }
            )
        }
    },
    useOrmPlugin: true
})