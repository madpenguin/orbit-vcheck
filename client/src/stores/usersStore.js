import { defineStore } from 'pinia'

export const useUsersStore = defineStore('usersStore', {
    state: () => {
        return {
            model: 'users'
        }
    },
    actions: {
        sort (params) {},
        populate () {
            const label = 'populate'
            if (label in this.local_ids) {
                console.log(`query already active: ${this.model}/${label}`)
                return
            }
            const method = `api_${this.model}_get_ids`
            this.query({
                model: this.model,
                label: label,
                method: method},
                (response) => {
                    if (!response || !response.ok)
                        throw new Error(response ? response.toString : `no query: ${method}`)
                    // else console.log('Populate>', response)
                }
            )
        }
    },
    useOrmPlugin: true
})