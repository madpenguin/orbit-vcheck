import { defineStore } from 'pinia'

export const useCommonStore = defineStore('commonStore', {
    state: () => {
        return {
            authenticated: false,
            connected: false,
            timenow60: new Date(),
            timenow: new Date(),
            host_id: '',
            erase: false,
            error: null,
        }
    },
    actions: {
        start_timer () {
            setInterval(() => {this.timenow = new Date()}, 1000)
            setInterval(() => {this.timenow60 = new Date()}, 60000)
            return this
        },
    },
    useOrmPlugin: false
})