import { createApp, reactive } from 'vue'
import { createPinia } from 'pinia'
import naive from "naive-ui"
import App from './App.vue'
import router from './router'
import './assets/main.css'
import "@fontsource/amaranth"
import "@fontsource/archivo"
import "@fontsource/league-gothic"

import OrmBase from '@/library/OrmBase.js'
import ZeroDocs from '@/components/zerodocs/index.js';

const app = createApp(App)
const pinia = createPinia()

app.config.unwrapInjectedRef = true

pinia.use(OrmBase)
app.use(naive)
app.use(pinia)
app.use(router)
app.use(ZeroDocs)
app.mount('#app')
