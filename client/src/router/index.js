import { createRouter, createWebHashHistory } from 'vue-router'
import Users from '@/views/Users.vue'
import Sessions from '@/views/Sessions.vue'
import Versions from '@/views/Versions.vue'
import ZeroDocs from '@/components/zerodocs/ZeroDocs.vue';

const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    { path: '/'                           , redirect: '/home' },
    { path: '/sessions' , name: 'sessions', component: Sessions },
    { path: '/users'    , name: 'users'   , component: Users },
    { path: '/versions' , name: 'versions', component: Versions },
    { path: '/api'      , name: 'api'     , component: ZeroDocs, props: {root: 'api'} , id: 1 },
    { path: '/home'     , name: 'home'    , component: ZeroDocs, props: {root: 'home'}, id: 2 },
  ]
})
export default router
