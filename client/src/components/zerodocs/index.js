import zerodocs from './ZeroDocs.vue'

const plugin = {
    install (app, options) {
        app.component('ZeroDocs', zerodocs)
    }
}

export default plugin