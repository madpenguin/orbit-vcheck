import { h } from 'vue'
import { defineStore } from 'pinia'
import { NIcon } from 'naive-ui'
import { useCacheStore } from '@/components/zerodocs/stores/cacheStore.js'
import { GithubFilled, GitlabFilled, GitlabOutlined } from '@vicons/antd'

export const useProjectStore = defineStore('projectStore', {
    state: () => {
        return {
            model: 'projects',
            tree: [],
            lookup: new Map(),
            selected: [],
            expanded: [],
            checked: [],
            cacheStore: null,
            current_node: null,
            current_stamp: 0,
        }
    },
    getters: {
        project_ids: (state) => 'populate' in state.local_ids ? state.local_ids['populate'] : []
    },
    actions: {
        hook_init (self) {
            this.cacheStore = useCacheStore()
        },
        sort (params) {},
        hook_store_post (response) {
            if (!response.data || !response.data.length) {
                console.log("No Data")
                return
            }
            response.data.forEach(item => {
                if (this.lookup.has(item.key)) {
                    let data = this.lookup.get(item.key)
                    let index = data.index
                    this.tree[index] = {...data, ...item}
                } else {
                    this.addTreeEntry(item)
                }
                // this.cacheStore.addTreeRoot({...item})
            })
        },
        removeNode (node) {
            if (node.children) {
                node.children.forEach(item => this.removeNode(item))
            }
            this.lookup.delete(node.key)
            if (typeof node.index != 'undefined') delete this.tree[node.index]
            this.cacheStore.delTreeRoot(node)
            console.log("Delete: ", node.label)
        },
        hook_invalidate (response) {
            response.data.forEach(id => {
                console.log("Remove tree: ", id)
                let data = this.data.get(id)
                this.removeNode(this.lookup.get(data.key))
            })
            if (this.tree.length && this.tree[0]) {
                console.log("Setting selected", this.tree.length)
                this.selected = [this.tree[0].key]
            }
        },
        getProjectId (provider, project, callback) {
            this._socket.emit('get_project_id', {provider: provider, project: project}, (response) => {
                console.log("Response>", response)
                return callback(response)
            })
        },
        add (params, callback) {
            this._socket.emit('get_project_id', params, (response) => {
                if (!response||!response.ok) return callback(response)
                params.project_id = response.id
                this._socket.emit('project_put', params, (response) => {
                    return callback(response)
                })    
            })
        },
        addTreeEntry (item) {
            // console.log("Addtreeentry>", item)
            item.isLeaf = false
            item.path = ''
            item.key = item._id
            item.index = this.tree.length
            let cls, icon
            switch (item.provider) {
                case 'gitlab':
                    cls = 'ext-gitlab'
                    icon = GitlabFilled
                    break
                case 'github':
                    cls = 'ext-github'
                    icon = GithubFilled
                default:
                    break
            }
            item.prefix = () => h(NIcon, {class: cls}, {default: () => h(icon)}),
            this.tree.push(item)
            this.lookup.set(item.key, item)
        },
        populate (root, tree, callback=null) {
            const label = 'populate'
            if (label in this.local_ids) {
                console.log(`query already active: ${this.model}/${label}`)
                return
            }
            const method = `api_${this.model}_get_ids`
            this.query({model: this.model, label: label, method: method}, (response) => {
                if (!response || !response.ok)
                    throw new Error(response ? response.toString : `no query: ${method}`)
                if (callback) callback(response)
            })
        },
        remove (params, callback=null) {
            this._socket.emit('project_remove', params, (response) => {
                if (callback) callback(response)
            })
        }
    },
    useOrmPlugin: true
})
