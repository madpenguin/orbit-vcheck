import { defineStore } from 'pinia'

export const useCacheStore = defineStore('cacheStore', {
    state: () => {
        return {
            model: 'cache',
            path_forward: new Map(),
            path_reverse: new Map(),
        }
    },
    actions: {
        hook_init (self) {
            this.create_lookup('by_path', this.path_forward, this.path_reverse, (d) => `${d.provider}/${d.project}/${d.path}`.toLowerCase())
        },
        sort (params) {},
        get_api_remote (provider, api, path, callback) {
            this._socket.emit('get_api_remote', provider, api, path, (response) => {
                if (response && response.ok) callback(response)
                else console.warn('call failed :: ', response)
            })
        },
        // hook_invalidate (response) {
        //     response.data.forEach(id => {
        //         this.remove_tree(id)
        //     })
        // },
        cache_put (to_create, to_delete, callback=null) {
            this._socket.emit('cache_put', to_create, to_delete, (response) => {
                if (callback) callback(response)
            })
        },
        get_project_id (params, callback=null) {
            this._socket.emit('get_project_id', params, (response) => {
                if (callback) callback(response)
            })
        },
        fetch (params, force, callback=null) {
            this._socket.emit('cache_fetch', params, force, (response) => {
                if (callback) callback(response)
            })
        },
        update (old_data, new_data) {
            this._socket.emit('cache_put', old_data, new_data, (response) => {
                if (!response||!response.ok) this.error = response.error
            })
        }
    },
    useOrmPlugin: true
})
