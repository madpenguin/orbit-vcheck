# Orbit Versioncheck

This application has two purposes. Firstly it serves as a version notification and tracking
system for Orbit Communicator (or indeed any Orbit Framework based application) and secondly
it's a demonstration of how to write an Orbit Framework based application.

## What does it do?

It consists of a backend (written in Python) that provides a websockets API interface that
allows applications to discover the latest version of any published software components. Moreover
it allows applications to maintain a persistent connection over which they can be notified in 
real time when a new version becomes available. Orbit communicator for example will pop an
update button immediately a new version becomes available.

It also provides a User Interface (which can be made public, or not) which allows people to see
what's going on while providing a dynamic demonstration of some of the things the Framework can
facilitate.

<img style="height:260px" src="https://gitlab.com/madpenguin/orbit-vcheck/-/raw/main/screenshots/vcheck2.png" />
<img style="height:260px" src="https://gitlab.com/madpenguin/orbit-vcheck/-/raw/main/screenshots/vcheck1.png" />

## Other features

The <b>Home</b> page used a caching facility that fetches a <b>Markdown</b> page directly from
a source repository and renders it in HTML. (which would be <i>THIS</i> page) This is handy from
the perspective of reducing documentation, and indeed making documentation easier to write and
maintain. In this case I've written a README.md for the version-check project, which can then
be rendered as an online home page via an Orbit application.

## ToDo

Issues;

1.  Factor root and branch into indexes
2.  Make vcheck into module / reduce root
3.  Auto-add icons to toolbar

/Cache.py", line 160, in update
AttributeError: 'HtmlFormatter' object has no attribute 'is_xml'

.. trying to view table.py

*   Link API fn to source
*   Global search
*   Side Drawer
    - CPU Chart

*   Introduce "admin" user level
*   Refresh and Edit should be admin only
*   Add a "preload" option with a force refresh


* Add "last seen" to users
* Sidebar / CPU
* Webhooks for dynamic updates
* Need a fix for images / set width/height

curl https://api.github.com/repos/lassana/continuous-audiorecorder/contents/

....
  